# Design Pattern Code Exercise. 

> A small calculator app to show a usage of the decorator pattern in JS.

##### Online Preview at: [http://test.pmpak.com/avaloq/design-pattern-decorator/](http://test.pmpak.com/avaloq/design-pattern-decorator/)

## Summary
For the sake of simplicity this application does not use any build tool and the syntax is ES5. Also Vue.js was used so the source won't get polluted with DOM related functions.

**This is not intended to be a full blown calculator application, but just a dead simple case of how the decorator pattern can be used.**

## Local setup
No setup required, just open index.html in a browser.
## Additional libraries and plugins that were used
* [Vue.js (cdn version)](https://vuejs.org/v2/guide/#Getting-Started)
* [Array.prototype.find.polyfill (For Internet Explorer)](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/find)

## Browsers Tested
* Chrome 67.0.3396.99
* Firefox 61.0.1
* Edge 42.17134.1.0
* Internet Explorer 11