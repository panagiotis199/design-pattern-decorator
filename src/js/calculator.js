/**
 * A small and quick demonstration of decorator pattern.
 *
 * For the sake of simplicity a build setup is not used
 * for this demo.
 */

// First we create a base js object that we will use as a base for adding additional
// functionality to other instances
var calculator = function () {
    this.calculationsAvailable = [
        {symbol: '+', func: this.add},
        {symbol: '-', func: this.subtract}
    ];
};

// Simple add and subtract.
calculator.prototype = {

    add: function (x, y) {
        return x + y;
    },

    subtract: function (x, y) {
        return x - y;
    },

    calculate: function(operator, x, y) {

        var indexOfOperator = this.calculationsAvailable.indexOf(operator);

        if (!x || !y ) {
            return x || y || 0;
        }

        if (indexOfOperator === -1) {
            console.error('Unknown operator');
            return null;
        }

        var calculation = this.calculationsAvailable.find(function (calculation) {
            return calculation === operator;
        });

        return calculation.func(x, y);
    }
};

// extend the functionality of calculator without affecting the parent
var advancedCalculator = function (calculator) {
    this.calculator = calculator;

    this.calculationsAvailable = [
        {symbol: '*', func: this.multiply},
        {symbol: '/', func: this.division}
    ].concat(calculator.calculationsAvailable);
};

advancedCalculator.prototype = {

    add: function (x, y) {
        return this.calculator.add(x, y);
    },

    subtract: function (x, y) {
        return this.calculator.subtract(x, y);
    },

    calculate: function (operator, x, y) {
        return this.calculator.calculate.apply(this, [operator, x, y]);
    },

    multiply: function (x, y) {
        return x * y;
    },

    division: function (x, y) {

        if (y === 0) {
            console.log('Caution, Division with 0');
            return 0;
        }

        return x / y;
    }
};